﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Leermoment.Models
{
    public class Question
    {
        public int Id { get; set; }
        [Required]
        public Exam Exams { get; set; }
        [Required]
        public string QuestionText { get; set; }
    }
}