﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Leermoment.Models
{
    public class Exam
    {
        public int Id { get; set; }
        [Required]
        [StringLength(55)]
        public string Name { get; set; }
        [Required]
        [StringLength(255)]
        public string Description { get; set; }
        public int Questions { get; set; }
        [Required]
        public int StudentsEnrolled { get; set; }

        [Range(1, 5)]
        public int Rating { get; set; }

    }
}