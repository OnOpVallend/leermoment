﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Leermoment.Models
{
    public class ExamDesign
    {
        public int Id { get; set; }
        [Required]
        public Exam Exams { get; set; }
        [Required]
        public int BoxSize { get; set; }

        public string PictureLink { get; set; }
        public string PictureType { get; set; }
    }
}