﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Leermoment.Models
{
    public class Choice
    {
        public int Id { get; set; }
        [Required]
        public Question Questions { get; set; }
        [Required]
        public string ChoiceText { get; set; }
    }
}