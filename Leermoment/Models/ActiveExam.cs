﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Leermoment.Models
{
    public class ActiveExam
    {
        public int Id { get; set; }
        [Required]
        public Exam Exams { get; set; }
        [Required]
        public string ActiveFor { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        [Range(1,100)]
        public int Progress { get; set; }
        [Required]
        public int AtQuestion { get; set; }

    }
}