﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Leermoment.Startup))]
namespace Leermoment
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
